export default function sketch(p) {

    let width = 600;
    let height = 400;
    let rotation = 0;

    p.setup = function () {
        p.createCanvas(600, 400, p.WEBGL);

    };

    p.myCustomRedrawAccordingToNewPropsHandler = function (props) {
        if (props.width !== width)
        {
            p.resizeCanvas(props.width,height);
            width = props.width;
        }
        if (props.rotation) {
            rotation = props.rotation * Math.PI / 180;
        }
    };

    p.draw = function () {
        rotation++;
        p.background(100);
        p.noStroke();
        p.push();
        p.rotateY(rotation);
        p.box(100);
        p.pop();
    };

    p.receiveProps = (nextProps) => {
        console.log(nextProps)
    };

    p.unmount = () => {
        console.log('The sketch was unmounted.');
    }
};