import React, { Component } from 'react'
import P5Wrapper from './P5Wrapper';

import sketch from './sketch'

export default class P5comp extends Component {


    constructor(props) {
        super(props);
        this.wrapref = React.createRef();
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    }


    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions, false);
        console.log("Mount")
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
        console.log("Umount")
    }

    updateWindowDimensions() {
        this.props.HandleResize(this.wrapref.current.offsetWidth);
    }


    render() {
        let rotation = 45;
        return (
            <div ref={this.wrapref} style={{ width: "100%" }}>
                <P5Wrapper sketch={sketch} rotation={rotation} width={this.props.width} />
            </div>

        )
    }
}