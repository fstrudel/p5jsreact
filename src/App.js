import React, { Component } from 'react';

import P5comp from './p5comp/P5comp'

import logo from './logo.svg';
import './App.css';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = { visible: false, width: 600 };
  }



  handleClick = (e) => {
    e.preventDefault();
    console.log(this.state);
    this.setState((prevState) => ({ visible: !prevState.visible }));
  }


  handleResize = (width) => {
    this.setState((prevState) => ({ width: width }));
  }


  render() {
    return (
      <div className="App">
        <div className="App-intro">
            <button onClick={this.handleClick}>Show</button>
          {this.state.visible && <P5comp width={this.state.width} HandleResize={this.handleResize}></P5comp>}
        </div>
      </div>
    );
  }
}

export default App;
